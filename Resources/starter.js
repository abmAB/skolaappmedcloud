var win= Ti.UI.currentWindow;

Cloud.Objects.create({
    classname: 'cars',
    fields: {
        make: 'nissan',
        color: 'blue',
        year: 2005
    }
}, function (e) {
    if (e.success) {
        var car = e.cars[0];
        alert('Success:\n' +
            'id: ' + car.id + '\n' +
            'make: ' + car.make + '\n' +
            'color: ' + car.color + '\n' +
            'year: ' + car.year + '\n' +
            'created_at: ' + car.created_at);
    } else {
        alert('Error:\n' +
            ((e.error && e.message) || JSON.stringify(e)));
    }
});
                
