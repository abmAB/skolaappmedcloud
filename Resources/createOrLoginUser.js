Ti.include('settings.js');

var win=Ti.UI.currentWindow;
	
//win.layout = 'vertical';

var myMail=Ti.App.Properties.getString('mail','');


var createNewUser= Ti.UI.createLabel({
	top: '200dp',
	text: 'Skapa Användare'
});

var addKid= Ti.UI.createLabel({
	top: '230dp',
	text: 'Lägg till barn...'
});


var logIn= Ti.UI.createLabel({
	top:'170dp',
	text: 'Logga in',
});

var next= Ti.UI.createLabel({
	top:'240dp',
	text: 'Nästa...',
});

var passTF = Ti.UI.createTextField({
	top : '40dp',
	width : '220dp',
	height : '50dp',
	hintText : 'password...',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	paddingLeft : '5dp',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	passwordMask : true,
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});

var mailTF = Ti.UI.createTextField({
	value: myMail,
	top : '100dp',
	width : '220dp',
	height : '50dp',
	hintText : 'mail...',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	//keyboardType: Ti.UI.KEYBOARD_EMAIL,
	paddingLeft : '5dp',
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});

var firstNameTF = Ti.UI.createTextField({
	top : '100dp',
	width : '220dp',
	height : '50dp',
	hintText : 'Förnamn...',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	paddingLeft : '5dp',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});


var lastNameTF = Ti.UI.createTextField({
	top : '160dp',
	width : '220dp',
	height : '50dp',
	hintText : 'Efternamn...',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	//keyboardType: Ti.UI.KEYBOARD_EMAIL,
	paddingLeft : '5dp',
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});

var kidNameArrayTF= [];

for(i=0; i<8; i++){

    kidNameArrayTF[i] = Ti.UI.createTextField({
	top : '40dp',
	width : '220dp',
	height : '50dp',
	hintText : 'Barnets namn...',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	paddingLeft : '5dp',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});

win.add(kidNameArrayTF[i]);
kidNameArrayTF[i].hide();
 
 logg('count'+ i);
};


var streetNameTF = Ti.UI.createTextField({
	top : '40dp',
	width : '220dp',
	height : '50dp',
	hintText : 'Gata och nr...',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	paddingLeft : '5dp',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});

var phoneNumberTF = Ti.UI.createTextField({
	top : '160dp',
	width : '220dp',
	height : '50dp',
	hintText : 'Mobil nr...',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	//keyboardType: Ti.UI.KEYBOARD_EMAIL,
	paddingLeft : '5dp',
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});

var passConfTF = Ti.UI.createTextField({
	top : '100dp',
	width : '220dp',
	height : '50dp',
	hintText : 'password igen...',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	paddingLeft : '5dp',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	passwordMask : true,
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});

var userNameTF = Ti.UI.createTextField({
	top : '160dp',
	width : '220dp',
	height : '50dp',
	hintText : 'User name...',
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
	clearOnEdit : true,
	enableReturnKey : true,
	paddingLeft : '5dp',
	backgroundColor : '#fff',
	borderWidth : '2dp',
	borderColor : '#000',
	borderRadius : 10,
	textAlign:'center'

});




win.add(mailTF);
win.add(passTF);
win.add(firstNameTF);
win.add(lastNameTF);
//win.add(kidNameTF);
win.add(phoneNumberTF);
win.add(streetNameTF);
//win.add(next);
win.add(logIn);
win.add(createNewUser);
win.add(addKid);
win.add(passConfTF);
win.add(userNameTF);



addKid.hide();
firstNameTF.hide();
lastNameTF.hide();
passConfTF.hide();
userNameTF.hide();
streetNameTF.hide();
phoneNumberTF.hide();
//kidNameTF.hide();


logIn.addEventListener('click', function(e){
	
	Cloud.Users.login({
    login: mailTF.value,
    password: passTF.value
}, function (e) {
    if (e.success) {
        var user = e.users[0];
        alert('Success:\n' +
            'id: ' + user.id + '\n' +
            'sessionId: ' + Cloud.sessionId + '\n' +
            'first name: ' + user.first_name + '\n' +
            'last name: ' + user.last_name);
            
            win.close({modal: true});
            
    } else {
        alert('Error:\n' +
            ((e.error && e.message) || JSON.stringify(e)));
    }
});	
});

createNewUser.addEventListener('click', function(e){
	
	
	logIn.hide();
	createNewUser.hide();
	
	passTF.hide();
	mailTF.hide();
	
	firstNameTF.show();
	lastNameTF.show();
	//kidNameTF.show();
	kidNameArrayTF[0].show();
	addKid.show();
	

});

kidNameArrayTF[2].addEventListener('return', returnBtn);
firstNameTF.addEventListener('return', returnBtn);
lastNameTF.addEventListener('return', returnBtn); //{
	
	// firstNameTF.hide();
	// lastNameTF.hide();
	// streetNameTF.show();
	// phoneNumberTF.show();
	// mailTF.show();
	// Ti.App.Properties.setString('kid1',kidNameArrayTF[0].value);
	// Ti.App.Properties.setString('kid2',kidNameArrayTF[1].value);
	// Ti.App.Properties.setString('kid3',kidNameArrayTF[2].value);
	// Ti.App.Properties.setString('kid4',kidNameArrayTF[3].value);
	// Ti.App.Properties.setString('kid5',kidNameArrayTF[4].value);
	
	
	//});
	
function returnBtn (){
	
	firstNameTF.hide();
	lastNameTF.hide();
	streetNameTF.show();
	phoneNumberTF.show();
	mailTF.show();
	Ti.App.Properties.setString('kid1',kidNameArrayTF[0].value);
	Ti.App.Properties.setString('kid2',kidNameArrayTF[1].value);
	Ti.App.Properties.setString('kid3',kidNameArrayTF[2].value);
	Ti.App.Properties.setString('kid4',kidNameArrayTF[3].value);
	Ti.App.Properties.setString('kid5',kidNameArrayTF[4].value);
	
}	
	
	phoneNumberTF.addEventListener('return', function(e){
	
	streetNameTF.hide();
	phoneNumberTF.hide();
	mailTF.hide();
	
	passTF.show();
	passConfTF.show();
	userNameTF.show();
	
	
	});
	
	var kidCounter= 0;
	
	addKid.addEventListener('click', function(e){
		
	if (kidCounter==0){
		
	kidNameArrayTF[1].show();	
	kidNameArrayTF[0].hide();
	
	
	}
	
	
	else if (kidCounter==1) {
		kidNameArrayTF[2].show();
		kidNameArrayTF[1].hide();
		
		
	}
	
	else if (kidCounter==2) {
		kidNameArrayTF[3].show();
		kidNameArrayTF[2].hide();
		
	}
	
		else if (kidCounter==3) {
		kidNameArrayTF[4].show();
		kidNameArrayTF[3].hide();
		
	}
	
	
	kidCounter++;
	});
	
	

userNameTF.addEventListener('return', function(){
	
	
	
	logg(firstNameTF.value);
	
	if (userNameTF.value !== 'undefined' && passTF.value !== 'undefined' && passConfTF.value !== 'undefined') { 



		Cloud.Users.create({
			username : firstNameTF.value,
			email: mailTF.value,
			password : passTF.value,
			password_confirmation : passConfTF.value,
			first_name: firstNameTF.value,
			last_name: lastNameTF.value,
			custom_fields:{
				kid1: Ti.App.Properties.getString('kid1',''),
				kid2:Ti.App.Properties.getString('kid2',''),
				kid3:Ti.App.Properties.getString('kid3',''),
				kid4:Ti.App.Properties.getString('kid4',''),
				kid5:Ti.App.Properties.getString('kid5','')}
				
		
		

		}, function(e) {
			if (e.success) {
				user = e.users[0];
				logg('Created! ' + 'userID: ' + user.id, user.custom_fields.kid1, user.custom_fields.kid2 );
				
				
				
				Ti.App.Properties.setString('userName', firstNameTF.value);
				Ti.App.Properties.setString('password', passTF.value);
				Ti.App.Properties.setString('mail', mailTF.value);
				
				win.close({modal: true});
				
				
			
			logg( 'myMail' + mailTF.value);
			
				

			} else {
				if (e.error && e.message) {
					alert('Error :' + e.message);
				}
			}

		});

	}	
	
	
});

